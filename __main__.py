import vk_api
from collections import defaultdict, Counter
import json
from os import path, environ, getcwd, makedirs, listdir
from multiprocessing import Pool
import requests
import cv2
import cv2
import numpy as np
from itertools import combinations
from scipy.misc import imread
from scipy.linalg import norm
from scipy import sum, average

PHOTOS_DIR = path.join(getcwd(), 'photos')


def request_jap_posts_with_photos():
    login =    environ['VK_LOGIN']
    password = environ['VK_PASS']
    vk_session = vk_api.VkApi(login, password)

    try:
        vk_session.authorization()
    except vk_api.AuthorizationError as error_msg:
        print(error_msg)
        return

    tools = vk_api.VkTools(vk_session)

    wall = tools.get_all_iter('wall.get', 100, {'owner_id': -111961667})

    posts = defaultdict(list)

    for post in wall:
        id = post['id']

        attachments = post.get('attachments', None)

        if attachments is None:
            continue

        for attachment in attachments:
            if attachment['type'] != 'photo':
                continue

            posts[id].append(attachment['photo'])

    return posts


def get_posts_with_photos():
    posts_path = 'posts.json'

    if path.isfile(posts_path):
        with open(posts_path) as f:
            posts = json.load(f)
    else:
        print("requesting")
        posts = request_jap_posts_with_photos()
        with open(posts_path, 'w') as f:
            json.dump(posts, f)

    return posts


def get_all_photos_field(posts, field_name):
    buff = []
    for post_photos in posts.values():
        fields = [p[field_name] for p in post_photos]
        buff.extend(fields)
    return buff


def download_photo(url):

    """If file was downloaded successfully - return file_name"""

    file_name = url.split('/')[-1]

    file_path = path.join(PHOTOS_DIR, file_name)

    if path.isfile(file_path):
        return file_name

    r = requests.get(url, stream=True)

    if r.status_code != 200:
        return None

    with open(file_path, 'wb') as f:
        for chunk in r.iter_content():
            f.write(chunk)
    return file_name


def download_photos(urls):
    if path.exists(PHOTOS_DIR):
        return listdir(PHOTOS_DIR)
    else:
        makedirs(PHOTOS_DIR)

        pool = Pool()

        print("Downloading...")
        return pool.map(download_photo, urls)


def rmse(predictions, targets):
    n = len(predictions)
    return np.linalg.norm(predictions - targets) / np.sqrt(n)

def compare_images(img1, img2):
    # normalize to compensate for exposure difference
    img1 = normalize(img1)
    img2 = normalize(img2)
    # calculate the difference and its norms
    m_norm = cv2.norm(img1, img2, cv2.NORM_L1)
    # z_norm = norm(diff.ravel(), 0)  # Zero norm
    return m_norm

def to_grayscale(arr):
    "If arr is a color image (3D array), convert it to grayscale (2D array)."

    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

def normalize(arr):
    cv2.normalize(arr, arr, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    return arr


def is_similar(image_name1, image_name2):
    image_path1 = path.join(PHOTOS_DIR, image_name1)
    image_path2 = path.join(PHOTOS_DIR, image_name2)

    image1 = cv2.imread(image_path1)
    image2 = cv2.imread(image_path2)

    equal_shape = image1.shape == image2.shape
    if not equal_shape:
        return False

    n_m = compare_images(image1, image2)
    sim = n_m < 600000.0

    if sim:
        print(n_m)

    return sim


def generate_non_unique_images(images):

    for image_name1, image_name2 in combinations(images, 2):
        # cv2.imshow('image', image1)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        similar = is_similar(image_name1, image_name2)
        if similar:
            yield (image_name1, image_name2)


def find_photo(posts, photo_name):
    for id, post_photos in posts.items():
        for photo in post_photos:
            if photo["photo_130"].endswith(photo_name):
                return (id, photo["id"])


if __name__ == '__main__':
    posts = get_posts_with_photos()

    all_photos_130_urls = get_all_photos_field(posts, 'photo_130')

    photos = download_photos(all_photos_130_urls)
    for image_name1, image_name2 in generate_non_unique_images(photos):
        image1_post_id, image1_id = find_photo(posts, image_name1)
        image2_post_id, image2_id = find_photo(posts, image_name2)
        print("Photo https://vk.com/japan.cars.style?z=photo-111961667_{} \n\tin post https://vk.com/japan.cars.style?w=wall-111961667_{} \n\tequal to https://vk.com/japan.cars.style?z=photo-111961667_{} \n\tin post https://vk.com/japan.cars.style?w=wall-111961667_{}\n".format(image1_id,
                                                                  image1_post_id,
                                                                  image2_id,
                                                                  image2_post_id))
